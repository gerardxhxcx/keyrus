import 'bootstrap'

import './files'

import '../scss/main.scss'

import jQuery from "jquery";
window.$ = window.jQuery = jQuery;

const x = () => console.log('x')

x();

(function($){
   $('.btn').hover(function() {
      $(this).text('Entrar')
   }, function() {
      $(this).text('Login')
   });

   $('.btn[type="submit"]').on('click', function(e) {
      e.preventDefault()
      $('.alert-success, .alert-danger').hide()
      
      const regexEmail =  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

      let email = $('[name="email"]').val().trim(),
         pass = $('[name="password"]').val().trim()

      if(regexEmail.test(email) && pass != ""){
         $('.alert-success').fadeIn(300);
      }
      else{
         $('.alert-danger').fadeIn(300);
      }
   })
})(jQuery)
