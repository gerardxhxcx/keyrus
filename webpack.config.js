const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
   entry: {
      main: path.resolve(__dirname, 'assets/js/main.js')
   },
   output: {
      filename: 'main.min.js',
      path: path.resolve(__dirname, 'public', 'js'),
      publicPath: path.resolve(__dirname, 'public') + '/'
   },
   module: {
      rules: [
         {
            test: /\.(js|jsx)$/,
            use: {
               loader: 'babel-loader',
               options: {
                  presets: ['env'],
                  plugins: ['syntax-dynamic-import']
               }
            }
         }, {
            test: /\.(jpg|png|gif)$/,
            use: {
               loader: 'file-loader',
               options: {
                  name: '[name].[ext]',
                  outputPath: '../img/',
               }
            }
         }, {
            test: /\.(woff|woff2|eot|ttf|svg)$/,
            use: {
               loader: 'file-loader',
               options: {
                  name: '[name].[ext]',
                  outputPath: '../fonts/',
               }
            }
         }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
               use: [
                  {
                     loader: 'css-loader',
                     options: {
                        minimize: true
                     }
                  }
               ]
            })
         }, {
            test: /\.(scss)$/,
            use: ExtractTextPlugin.extract({
               fallback: 'style-loader',
               use: [
                  {
                     loader: 'css-loader',
                     options: {
                        url: false,
                        minimize: true,
                        sourceMap: false
                     }
                  }, {
                     loader: 'sass-loader',
                     options: {
                        sourceMap: false
                     }
                  }
               ]
            })
         }
      ]
   },
   plugins: [
      new ExtractTextPlugin("../css/[name].min.css"),
   ]

};
